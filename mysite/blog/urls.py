from django.urls import path
from blog import views

urlpatterns = [
    path('', views.demo, name='demo'),
    path('post/<int:pk>/', views.post_details, name='post_details'),
    path('post/new/', views.post_new, name='post_new'),
    path('about-us/', views.about, name='about'),
    path('contact/', views.contact, name='contact'),
    path('post/<int:pk>/edit/', views.post_edit, name='post_edit'),

]
