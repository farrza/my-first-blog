from django.shortcuts import render, get_object_or_404, redirect
from blog.models import Post
from django.http import HttpResponse
from django.utils import timezone
from blog.forms import PostForm

# Create your views here.

def demo(request):
	return render(request, 'blog/demo.html', {})

def post_list(request):
    print("Post list")
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-created_date')
    return render(request, "blog/post_list.html", {'posts': posts})

def post_details(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, "blog/post_details.html", {'post': post})

def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_details', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_details', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})


def about(request):
    print("Testing about view")
    return HttpResponse("Bla bla")


def contact(request):
    print("Test")
    return HttpResponse("Bla sla")
